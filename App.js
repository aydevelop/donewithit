import React, { useState, useEffect } from "react";
import { Platform, Text, View, StyleSheet, Button } from "react-native";
import Screen from "./components/Screen";
import { createStackNavigator } from "@react-navigation/stack";
import {
  NavigationContainer,
  useFocusEffect,
  useNavigation,
} from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import AuthNavigator from "src/navigation/AuthNavigator";
import RegisterScreen from "./screens/RegisterScreen";
import LoginScreen from "./screens/LoginScreen";
import navigationTheme from "./navigation/navigationTheme";
import AppNavigator from "./navigation/AppNavigator";
import ListingEditScreen from "./screens/ListingEditScreen";
import NetInfo, { useNetInfo } from "@react-native-community/netinfo";
import AuthContext from "./auth/context";
import authStorage from "./auth/storage";
import jwtDecode from "jwt-decode";
import { Asset } from "expo-asset";
import AppLoading from "expo-app-loading";
import { navigationRef } from "./navigation/RootNavigation";

export default function App() {
  const [user, setUser] = useState(null);
  const [isReady, setIsReady] = useState(false);

  const restoreToken = async () => {
    const token = await authStorage.getToken();
    if (!token) return;

    try {
      let res = await jwtDecode(token);
      setUser(res);
    } catch (error) {
      console.log("jwtDecode error: " + error);
    }
  };

  // useEffect(() => {
  //   if (!user) {
  //     restoreToken();
  //   }
  // }, []);

  if (!isReady) {
    return (
      <AppLoading
        startAsync={restoreToken}
        onFinish={() => setIsReady(true)}
        onError={console.warn}
      />
    );
  }

  return (
    <AuthContext.Provider value={{ user, setUser }}>
      <NavigationContainer ref={navigationRef} theme={navigationTheme}>
        {!!user ? <AppNavigator /> : <AuthNavigator />}
      </NavigationContainer>
    </AuthContext.Provider>
  );
}
