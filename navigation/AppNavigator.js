import React, { useState, useEffect } from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import AccountScreen from "../screens/AccountScreen";
import ListingScreen from "../screens/ListingScreen";
import FeedNavigator from "./FeedNavigator";
import AccountNavigator from "./AccountNavigator";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import NewListingButton from "./NewListingButton";
import { useNavigation } from "@react-navigation/native";
import ListingEditScreen from "../screens/ListingEditScreen";
//import { Notifications } from "expo";
import * as Notifications from "expo-notifications";
import * as Permissions from "expo-permissions";
import expoPushTokensApi from "../api/expoPushTokens";
import * as RootNavigation from "./RootNavigation";
const Tab = createBottomTabNavigator();

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  }),
});

const AppNavigator = ({ navigation }) => {
  const notificationListener = React.useRef();
  const responseListener = React.useRef();

  React.useEffect(() => {
    registerForPushNotifications();

    function sayHi(phrase, who) {
      RootNavigation.navigate("Account");
    }

    setTimeout(sayHi, 1000, "Приве4т", "Джон");

    notificationListener.current = Notifications.addNotificationReceivedListener(
      (notification) => {
        console.log("__________________________________");
        console.log(notification.request.content.title);
        console.log(notification.request.content.body);
        console.log(notification.request.content.data);
      }
    );

    responseListener.current = Notifications.addNotificationResponseReceivedListener(
      (response) => {
        console.log(response);
      }
    );

    return () => {
      Notifications.removeNotificationSubscription(notificationListener);
      Notifications.removeNotificationSubscription(responseListener);
    };

    // responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
    //   console.log(response);
    // });

    // return () => {
    //   Notifications.removeNotificationSubscription(notificationListener);
    //   Notifications.removeNotificationSubscription(responseListener);
    // };
  }, []);

  const registerForPushNotifications = async () => {
    try {
      const permission = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      if (!permission.granted) return;
      const token = await Notifications.getExpoPushTokenAsync();
      expoPushTokensApi.register(token.data);
      console.log("Token  " + token.data);
    } catch (error) {
      console.log("Error   " + error);
    }
  };

  return (
    <Tab.Navigator tabBarOptions={{ style: { height: 50, paddingBottom: 5 } }}>
      <Tab.Screen
        name="Feed"
        component={FeedNavigator}
        options={{
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="home" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        options={(navigation) => ({
          // tabBarButton: () => (
          //   <NewListingButton
          //     onPress={() => {
          //       const navigation = useNavigation();

          //       var res = Object.getOwnPropertyNames(object).filter(function (
          //         property
          //       ) {
          //         return typeof object[property] == "function";
          //       });

          //       alert(res);
          //       //navigation.push("Account");
          //     }}
          //   />
          // ),
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons
              name="plus-circle"
              color={color}
              size={size * 2.7}
            />
          ),
        })}
        name="Add"
        component={ListingEditScreen}
      />
      <Tab.Screen
        options={{
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons name="account" color={color} size={size} />
          ),
        }}
        name="Account"
        component={AccountNavigator}
      />
    </Tab.Navigator>
  );
};

export default AppNavigator;
