import client from "./client";
const endpoint = "/listings";

const getListings = () => client.get("/listings");

export const addListing = (listing) => {
  const data = new FormData();
  data.append("title", listing.title);
  data.append("price", listing.price);
  data.append("categoryId", listing.category?.value);
  data.append("description", listing.description);

  listing.images.forEach((image, index) =>
    data.append("images", {
      name: "image" + index,
      type: "image/jpeg",
      uri: image,
    })
  );

  return client.post(endpoint, data, {
    onDownloadProgress: (process) =>
      console.log(process.loaded / process.total),
  });
};

export default {
  addListing,
  getListings,
};
