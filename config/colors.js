export default {
  primary: "#D82E2F",
  secondary: "#1B98F5",
  black: "#0D0D0D",
  white: "#fff",
  medium: "#6e6969",
  light: "#ECF0F1",
  danger: "#ff5252",
  dark: "#0c0c0c",
  red: "#e50000",
};
