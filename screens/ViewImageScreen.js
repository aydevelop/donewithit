import React from 'react';
import { StyleSheet, Text, View, Image, StatusBar } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import colors from '../config/colors';

export default function ViewImageScreen() {
  return (
    <View style={styles.container}>
      <StatusBar hidden={true} />
      <View style={styles.closeIcon}>
        <MaterialCommunityIcons name='close' color='white' size={40} />
      </View>
      <View style={styles.deleteIcon}>
        <MaterialCommunityIcons
          name='trash-can-outline'
          color='white'
          size={40}
        />
      </View>
      <Image
        resizeMode='contain'
        style={styles.image}
        source={require('../assets/app/chair.jpg')}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.black,
  },
  image: {
    height: '100%',
    width: '100%',
  },
  closeIcon: {
    // width: 51,
    // height: 51,
    // backgroundColor: colors.primary,
    position: 'absolute',
    top: 10,
    left: 10,
  },
  deleteIcon: {
    // width: 51,
    // height: 51,
    // backgroundColor: colors.secondary,
    position: 'absolute',
    top: 10,
    right: 10,
  },
});
