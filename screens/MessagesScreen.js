import React, { useState } from "react";
import ListItem from "../components/ListItem";
import { SafeAreaView, View, FlatList, StyleSheet, Text } from "react-native";
import Constants from "expo-constants";
import Screen from "../components/Screen";
import ListItemDeleteAction from "../components/ListItemDeleteAction";
import colors from "../config/colors";

const initMessages = [
  {
    id: 1,
    title:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut viverra lectus et libero imperdiet dignissim. Interdum et malesuada fames ac ante ipsum primis in faucibus. Quisque luctus nulla lectus. Suspendisse cursus convallis massa. Duis id elit arcu. Nulla egestas metus sed augue vestibulum maximus. Nulla ex urna, ornare sed auctor porta, convallis id diam. Proin vestibulum pulvinar sapien ut lobortis. Suspendisse congue ex vitae enim ornare tincidunt eget ut odio. Maecenas scelerisque diam sapien, vitae cursus enim vestibulum at. Morbi aliquet nec nisi eget semper. Etiam aliquet commodo mi nec tempor. Vivamus felis sapien, volutpat sit amet enim et, sollicitudin maximus erat. Nullam id volutpat quam. Nam ac fermentum nulla, eget facilisis dui. ",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut viverra lectus et libero imperdiet dignissim. Interdum et malesuada fames ac ante ipsum primis in faucibus. Quisque luctus nulla lectus. Suspendisse cursus convallis massa. Duis id elit arcu. Nulla egestas metus sed augue vestibulum maximus. Nulla ex urna, ornare sed auctor porta, convallis id diam. Proin vestibulum pulvinar sapien ut lobortis. Suspendisse congue ex vitae enim ornare tincidunt eget ut odio. Maecenas scelerisque diam sapien, vitae cursus enim vestibulum at. Morbi aliquet nec nisi eget semper. Etiam aliquet commodo mi nec tempor. Vivamus felis sapien, volutpat sit amet enim et, sollicitudin maximus erat. Nullam id volutpat quam. Nam ac fermentum nulla, eget facilisis dui. ",
    image: require("../assets/app/mosh.jpg"),
  },
  {
    id: 2,
    title: "T2",
    description: "D2",
    image: require("../assets/app/mosh.jpg"),
  },
];

const MessagesScreen = () => {
  const [messages, setMessages] = useState(initMessages);
  const [refreshing, setRefreshing] = useState(false);

  const handleDelete = (message) => {
    const newMessages = messages.filter((item) => item.id !== message.id);
    setMessages(newMessages);
  };

  const renderItem = ({ item }) => (
    <ListItem
      title={item.title}
      subTitle={item.description}
      image={item.image}
      onPress={() => console.log(item)}
      renderRightActions={() => (
        <ListItemDeleteAction onPress={() => handleDelete(item)} />
      )}
    />
  );

  return (
    <Screen>
      <FlatList
        data={messages}
        renderItem={renderItem}
        keyExtractor={(item) => item.id.toString()}
        ItemSeparatorComponent={() => (
          <View
            style={{ width: "100%", height: 1, backgroundColor: "#FF6263" }}
          />
        )}
        refreshing={refreshing}
        onRefresh={() => {
          setMessages(initMessages);
        }}
      />
    </Screen>
  );
};

const styles = StyleSheet.create({});

export default MessagesScreen;
