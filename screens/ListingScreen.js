import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, FlatList } from "react-native";
import Screen from "../components/Screen";
import Card from "../components/Card";
import colors from "../config/colors";
import listingsApi from "../api/listings";
import Button from "../components/AppButton";
import AppText from "../components/AppText";

const DATA = [
  {
    id: 1,
    title: "Red jacket for sale",
    price: 100,
    image: require("../assets/app/jacket.jpg"),
  },
  {
    id: 2,
    title: "Couch 123 in great condition",
    price: 200,
    image: require("../assets/app/couch.jpg"),
  },
];

const ListingScreen = ({ navigation }) => {
  const [listings, setListings] = useState([]);
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);

  function sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  const loadListings = async () => {
    try {
      setLoading(true);
      //await sleep(5000);
      const response = await listingsApi.getListings();
      setLoading(false);

      //console.log(JSON.stringify(response, null, 2));

      if (!response.ok) {
        return setError(true);
      }

      setError(false);
      setListings(response.data?.reverse());

      //console.log("losg:: ____________________________________________");
      //console.log(response.data);
    } catch (err) {
      console.log("ERROR:::: " + err);
    }
  };

  useEffect(() => {
    //loadListings();

    const unsubscribe = navigation.addListener("focus", () => {
      loadListings();
    });
    return unsubscribe;
  }, []);

  return (
    <Screen styles={styles.screen}>
      {error && (
        <>
          <AppText> Couldn't retrive the listings </AppText>
          <Button title="Retry" onPress={loadListings}></Button>
        </>
      )}
      {/* <ActivityIndicator visible={false} />} */}
      {loading && (
        <View>
          <Text style={{ fontSize: 17, margin: 10 }}>Loading.....</Text>
        </View>
      )}
      <FlatList
        data={listings}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
          <Card
            title={item.title}
            subTitle={"$" + item.price}
            imageUrl={item?.images[0]?.url}
            onPress={() => navigation.push("ListringsDetails", item)}
          />
        )}
      />
    </Screen>
  );
};

export default ListingScreen;
const styles = StyleSheet.create({
  screen: {
    padding: 10,
    backgroundColor: colors.light,
  },
});
