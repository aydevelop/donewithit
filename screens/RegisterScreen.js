import React, { useState } from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import { Formik } from "formik";
import * as Yup from "yup";

import Screen from "../components/Screen";
import AppTextInput from "../components/AppTextInput";
import AppButton from "../components/AppButton";
import AppText from "../components/AppText";
import Icon from "src/components/Icon";
import FormField from "../components/FormField";
import colors from "../config/colors";
import ErrorMessage from "../components/ErrorMessage";
import authApi from "../api/auth";
import useAuth from "../auth/useAuth";
import useApi from "../hooks/useApi";
import usersApi from "../api/users";
import ActivityIndicator from "../components/ActivityIndicator";

const validationScheme = Yup.object().shape({
  email: Yup.string().required().email().label("Email"),
  password: Yup.string().required().min(4).label("Password"),
  password2: Yup.string()
    .required()
    .min(4)
    .label("PasswordRepeat")
    .oneOf([Yup.ref("password"), null], "Passwords must match"),
});

const RegScreen = () => {
  const registerApi = useApi(usersApi.register);
  const loginApi = useApi(authApi.login);
  const auth = useAuth();
  const [error, setError] = useState();

  return (
    <Screen styles={styles.container}>
      <Image
        style={styles.logo}
        source={require("../assets/app/logo-red.png")}
      />

      <ErrorMessage error={error} visible={error} />
      <Formik
        validationSchema={validationScheme}
        initialValues={{ email: "", password: "", password2: "" }}
        onSubmit={async (userInfo) => {
          userInfo.name = "John Smith";
          userInfo.password2 = undefined;

          const result = await registerApi.request(userInfo);
          if (!result.ok) {
            if (result.data) setError(result.data.error);
            else {
              setError(
                "An unexpected error occurred.... " + JSON.stringify(result)
              );
              console.log(result);
            }
            return;
          }

          const { data: authToken } = await loginApi.request(
            userInfo.email,
            userInfo.password
          );

          //alert(authToken);
          auth.logIn(authToken);

          // const result = await authApi.register(userInfo);
          // if (!result.ok) {
          //   if (result.data) setError(result.data.error);
          //   else {
          //     setError("An unexpected error occurred.");
          //     console.log(result);
          //   }
          //   return;
          // } else {
          //   // const result = await authApi.login(
          //   //   userInfo.email,
          //   //   userInfo.password
          //   // );
          //   alert("ok");
          // }
        }}
      >
        {({ handleChange, handleSubmit, errors, touched, setFieldTouched }) => (
          <>
            {/* <ActivityIndicator
              visible={registerApi.loading || loginApi.loading}
            /> */}
            <AppTextInput
              onChangeText={handleChange("email")}
              onEndEditing={() => {
                setFieldTouched("email");
              }}
              autoCapitalize="none"
              icon="email"
              placeholder="Email"
              autoCapitalize="none"
              autoCorrect={false}
              keyboardType="email-address"
              textContentType="emailAddress"
            />
            <ErrorMessage error={errors.email} visible={touched.email} />

            <AppTextInput
              onChangeText={handleChange("password")}
              onEndEditing={() => {
                setFieldTouched("password");
              }}
              autoCapitalize="none"
              autoCorrect={false}
              icon="lock"
              placeholder="Password"
              secureTextEntry={true}
              textContentTyp="password"
            ></AppTextInput>
            <ErrorMessage error={errors.password} visible={touched.password} />

            <FormField
              autoCapitalize="none"
              autoCorrect={false}
              icon="lock"
              name="password2"
              placeholder="PasswordRepeat"
              secureTextEntry
              textContentType="password"
            />

            <AppButton
              color={colors.secondary}
              styles={{ marginTop: 30 }}
              title="Register"
              onPress={handleSubmit}
            />
          </>
        )}
      </Formik>
    </Screen>
  );
};

export default RegScreen;
const styles = StyleSheet.create({
  container: {
    margin: 10,
  },
  logo: {
    width: 80,
    height: 80,
    alignSelf: "center",
    marginTop: 20,
    marginBottom: 30,
  },
});
