import React, { useState } from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import { Formik } from "formik";
import * as Yup from "yup";
import Screen from "../components/Screen";
import AppTextInput from "../components/AppTextInput";
import AppButton from "../components/AppButton";
import AppText from "../components/AppText";
import Icon from "src/components/Icon";
import ErrorMessage from "../components/ErrorMessage";
import authApi from "../api/auth";
import jwtDecode from "jwt-decode";
import AuthContext from "../auth/context";
import authStorage from "../auth/storage";

const validationScheme = Yup.object().shape({
  email: Yup.string().required().email().label("Email"),
  password: Yup.string().required().min(4).label("Password"),
});

const LoginScreen = () => {
  const authContext = React.useContext(AuthContext);
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();

  const handleSubmit = async ({ email, password }) => {
    const result = await authApi.login(email, password);

    if (!result.ok) {
      alert("Error of login! " + result.data.error);
    }

    try {
      const user = await jwtDecode(result.data);
      authContext.setUser(user);
      authStorage.storeToken(result.data);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Screen styles={styles.container}>
      <Image
        style={styles.logo}
        source={require("../assets/app/logo-red.png")}
      />

      <Formik
        validationSchema={validationScheme}
        initialValues={{ email: "", password: "" }}
        onSubmit={handleSubmit}
      >
        {({ handleChange, handleSubmit, errors, touched, setFieldTouched }) => (
          <>
            <AppTextInput
              onChangeText={handleChange("email")}
              onEndEditing={() => {
                setFieldTouched("email");
              }}
              autoCapitalize="none"
              icon="email"
              placeholder="Email"
              autoCapitalize="none"
              autoCorrect={false}
              keyboardType="email-address"
              textContentType="emailAddress"
            />
            <ErrorMessage error={errors.email} visible={touched.email} />

            <AppTextInput
              onChangeText={handleChange("password")}
              onEndEditing={() => {
                setFieldTouched("password");
              }}
              autoCapitalize="none"
              autoCorrect={false}
              icon="lock"
              placeholder="Password"
              secureTextEntry={true}
              textContentTyp="password"
            ></AppTextInput>
            <ErrorMessage error={errors.password} visible={touched.password} />

            <AppButton
              styles={{ marginTop: 30 }}
              title="Login"
              onPress={handleSubmit}
            />
          </>
        )}
      </Formik>
    </Screen>
  );
};

export default LoginScreen;
const styles = StyleSheet.create({
  container: {
    margin: 10,
  },
  logo: {
    width: 80,
    height: 80,
    alignSelf: "center",
    marginTop: 20,
    marginBottom: 30,
  },
});
