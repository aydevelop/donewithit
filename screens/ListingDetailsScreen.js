import React from "react";
import { View, Image, StyleSheet } from "react-native";
import AppText from "./../components/AppText";
import colors from "./../config/colors";
import ListItem from "./../components/ListItem";

function ListingDetailsScreen({ route }) {
  const listing = route.params;

  return (
    <View>
      <Image style={styles.image} source={{ uri: listing.images[0].url }} />
      <View style={styles.details}>
        <AppText style={styles.title}>{listing.title}</AppText>
        <AppText style={styles.price}>${listing.price}</AppText>
        <View style={styles.userInfo}>
          <ListItem
            title="Title ...."
            subTitle="Subtitle ...."
            image={require("../assets/app/mosh.jpg")}
          />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  image: {
    width: "100%",
    height: 200,
  },
  details: {
    padding: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: "600",
  },
  price: {
    color: colors.secondary,
    fontWeight: "bold",
    fontSize: 20,
  },
  userInfo: {
    marginVertical: 30,
  },
});

export default ListingDetailsScreen;
