import React from "react";
import { StyleSheet, Text, View, ImageBackground, Image } from "react-native";
import AppButton from "./../components/AppButton";
import colors from "./../config/colors";

export default function WelcomeScreen({ navigation }) {
  return (
    <ImageBackground
      style={styles.background}
      source={require("../assets/app/background.jpg")}
    >
      <View style={styles.logoContainer}>
        <Text style={{ marginBottom: 20, fontSize: 21 }}>
          Sell What You Don't Need !!!
        </Text>
        <Image
          style={styles.logo}
          source={require("../assets/app/logo-red.png")}
        />
      </View>
      <View style={styles.buttonsContainer}>
        <AppButton title="Login" onPress={() => navigation.push("Login")} />
        <AppButton
          title="Register"
          color={colors.secondary}
          onPress={() => navigation.push("Register")}
        />
      </View>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
  },
  buttonsContainer: {
    padding: 20,
    marginBottom: 30,
    width: "100%",
  },
  loginButton: {
    width: "100%",
    height: 60,
    backgroundColor: "#fc5c65",
  },
  registerButton: {
    width: "100%",
    height: 60,
    backgroundColor: "#4ecdc4",
  },
  logo: {
    width: 100,
    height: 100,
  },
  logoContainer: {
    position: "absolute",
    top: 51,
    alignItems: "center",
  },
});
