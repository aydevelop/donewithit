import React, { useState } from "react";
import { StyleSheet, Text, View, ScrollView } from "react-native";
import * as Yup from "yup";

import listingsApi from "../api/listings";
import FormImagePicker from "../components/FormImagePicker";
import Screen from "./../components/Screen";
import AppTextInput from "../components/AppTextInput";
import AppButton from "../components/AppButton";
import AppPicker from "../components/AppPicker";
import * as Progress from "react-native-progress";

const categories = [
  {
    backgroundColor: "#fc5c65",
    icon: "floor-lamp",
    label: "Furniture",
    value: 1,
  },
  {
    backgroundColor: "#fd9644",
    icon: "car",
    label: "Cars",
    value: 2,
  },
  {
    backgroundColor: "#fed330",
    icon: "camera",
    label: "Cameras",
    value: 3,
  },
  {
    backgroundColor: "#26de81",
    icon: "cards",
    label: "Games",
    value: 4,
  },
  {
    backgroundColor: "#2bcbba",
    icon: "shoe-heel",
    label: "Clothing",
    value: 5,
  },
  {
    backgroundColor: "#45aaf2",
    icon: "basketball",
    label: "Sports",
    value: 6,
  },
  {
    backgroundColor: "#4b7bec",
    icon: "headphones",
    label: "Movies & Music",
    value: 7,
  },
  {
    backgroundColor: "#a55eea",
    icon: "book-open-variant",
    label: "Books",
    value: 8,
  },
  {
    backgroundColor: "#778ca3",
    icon: "application",
    label: "Other",
    value: 9,
  },
];

const ListingEditScreen = ({ navigation }) => {
  const [title, setTitle] = useState();
  const [price, setPrice] = useState();
  const [category, setCategory] = useState();
  const [description, setDesc] = useState();
  const [uris, setUris] = React.useState([]);
  const [loading, setLoading] = React.useState(false);

  const handleSubmit = async () => {
    let item = { title, price, category, description, images: uris };

    setLoading(true);
    const result = await listingsApi.addListing(item);
    if (!result.ok) {
      return alert("Could not save the listing.");
    }

    setLoading(false);
    navigation.navigate("Listrings");
  };

  return (
    <Screen styles={styles.container}>
      <ScrollView>
        <FormImagePicker
          uploadUris={(items) => {
            setUris(items);
          }}
          name="images"
        />

        <AppTextInput
          onChangeText={(text) => {
            setTitle(text);
          }}
          autoCapitalize="none"
          autoCorrect={false}
          placeholder="Title"
        ></AppTextInput>
        <AppTextInput
          onChangeText={(text) => {
            setPrice(text);
          }}
          autoCapitalize="none"
          autoCorrect={false}
          placeholder="Price"
        ></AppTextInput>
        <AppTextInput
          onChangeText={(text) => {
            setDesc(text);
          }}
          autoCapitalize="none"
          autoCorrect={false}
          placeholder="Description"
        ></AppTextInput>

        <AppPicker
          placeholder="Category"
          items={categories}
          selectedItem={category}
          onSelectItem={(item) => {
            setCategory(item);
          }}
        ></AppPicker>

        {loading && <Text>loading.........</Text>}

        <AppButton
          styles={{ marginTop: 10 }}
          title="Post..."
          onPress={handleSubmit}
        />
      </ScrollView>
    </Screen>
  );
};

export default ListingEditScreen;
const styles = StyleSheet.create({
  container: {
    padding: 15,
  },
});
