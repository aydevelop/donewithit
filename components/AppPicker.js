import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableWithoutFeedback,
  Modal,
  Button,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import defaultStyles from '../config/styles';
import AppText from '../components/AppText';

const AppPicker = ({
  icon,
  placeholder,
  items,
  onSelectItem,
  selectedItem,
  ...props
}) => {
  const [modalVisible, setModalVisible] = useState(false);

  return (
    <>
      <TouchableWithoutFeedback
        onPress={() => {
          setModalVisible(true);
        }}
      >
        <View style={styles.container}>
          {icon && (
            <MaterialCommunityIcons
              size={20}
              name={icon}
              color={defaultStyles.colors.black}
              style={styles.icon}
            />
          )}
          <AppText styles={styles.text}>
            {selectedItem ? selectedItem.label : placeholder}
          </AppText>
          {icon && (
            <MaterialCommunityIcons
              size={25}
              name='chevron-down'
              color={defaultStyles.colors.black}
              style={styles.icon}
            />
          )}
        </View>
      </TouchableWithoutFeedback>
      <Modal visible={modalVisible} animationType='slide'>
        <Button
          title='Close'
          onPress={() => {
            setModalVisible(false);
          }}
        />
        <FlatList
          data={items}
          keyExtractor={(item) => item.value.toString()}
          renderItem={({ item }) => (
            <PickerItem
              label={item.label}
              onPress={() => {
                setModalVisible(false);
                onSelectItem(item);
              }}
            />
          )}
        ></FlatList>
      </Modal>
    </>
  );
};

export default AppPicker;
const styles = StyleSheet.create({
  container: {
    backgroundColor: defaultStyles.colors.light,
    borderRadius: 25,
    flexDirection: 'row',
    width: '100%',
    padding: 15,
    marginVertical: 10,
    borderWidth: 1,
    alignItems: 'center',
  },
  icon: {
    marginRight: 15,
  },
  text: {
    flex: 1,
  },
});

function PickerItem({ label, onPress }) {
  return (
    <TouchableOpacity onPress={onPress}>
      <AppText styles={styles2.text}>{label}</AppText>
    </TouchableOpacity>
  );
}

const styles2 = StyleSheet.create({
  text: {
    padding: 20,
  },
});
