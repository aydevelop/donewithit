import React from "react";
import { StyleSheet, Text, View } from "react-native";
import ImageInputList from "./ImageInputList";

const FormImagePicker = ({ uploadUris }) => {
  const [uris, setUris] = React.useState([]);

  const handleAdd = (uri) => {
    let res = [...uris, uri];
    setUris(res);
    uploadUris(res);
  };

  const handleRemove = (uri) => {
    let res = uris.filter((imageUri) => imageUri !== uri);
    setUris(res);
  };

  return (
    <ImageInputList
      imageUris={uris}
      onAddImage={handleAdd}
      onRemoveImage={handleRemove}
    />
  );
};

export default FormImagePicker;

const styles = StyleSheet.create({});
