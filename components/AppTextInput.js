import React from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import defaultStyles from '../config/styles';

const AppTextInput = ({ icon, placeholder, width = '100%', ...props }) => {
  return (
    <View style={[styles.container, { width }]}>
      {icon && (
        <MaterialCommunityIcons
          size={20}
          name={icon}
          color={defaultStyles.colors.black}
          style={styles.icon}
        />
      )}
      <TextInput
        placeholder={placeholder}
        style={[defaultStyles.text, { padding: 12, flex: 1 }]}
        {...props}
      />
    </View>
  );
};

export default AppTextInput;
const styles = StyleSheet.create({
  container: {
    backgroundColor: defaultStyles.colors.light,
    borderRadius: 25,
    flexDirection: 'row',
    width: '100%',
    marginVertical: 10,
    borderWidth: 1,
    alignItems: 'center',
  },
  icon: {
    marginLeft: 20,
    marginRight: 15,
  },
});
