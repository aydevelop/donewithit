import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, Image, Alert } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import colors from "../config/colors";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";

import * as ImagePicker from "expo-image-picker";
import * as Permissions from "expo-permissions";

const ImageInput = ({ imageUri, onChangeImage }) => {
  const [imageUri2, setImageUri2] = useState(imageUri);

  useEffect(() => {
    requestPermission();
  }, []);

  const requestPermission = async () => {
    // Permissions.askAsync(
    //   Permissions.CAMERA_ROLL,
    //   Permissions.LOCATION
    // );

    const result = await ImagePicker.requestCameraPermissionsAsync();
    if (!result.granted) {
      alert("You need to enable permission to access the library");
    }
  };

  const selectImage = async () => {
    try {
      await requestPermission();

      const result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        quality: 0.5,
      });
      if (!result.cancelled) {
        if (onChangeImage) {
          onChangeImage(result.uri);
        }

        setImageUri2(result.uri);
      } else {
      }
    } catch (error) {
      console.log("Error reading an image.... " + error);
    }
  };

  const handlePress = () => {
    if (!imageUri) {
      selectImage();
    } else {
      Alert.alert(
        "Alert Title",
        "My Alert Msg",
        [
          {
            text: "Cancel",
            onPress: () => {},
            style: "cancel",
          },
          { text: "OK", onPress: () => onChangeImage(null) },
        ],
        { cancelable: false }
      );
    }
  };

  return (
    <TouchableWithoutFeedback onPress={handlePress}>
      <View style={styles.container}>
        {!imageUri && (
          <>
            {/* <Text style={{ marginHorizontal: 4 }}>IMAGE INPUT</Text>
             */}
            <MaterialCommunityIcons
              name="camera"
              color={colors.medium}
              size={40}
              style={{ margin: 5 }}
            />
          </>
        )}
        {imageUri && <Image style={styles.image} source={{ uri: imageUri2 }} />}
      </View>
    </TouchableWithoutFeedback>
  );
};

export default ImageInput;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.light,
    borderRadius: 15,
    justifyContent: "center",
    alignItems: "center",
    height: 120,
    borderWidth: 1,
    overflow: "hidden",
    margin: 3,
  },
  image: {
    height: 100,
    width: 100,
  },
});
