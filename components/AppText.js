import React from 'react';
import { Text, StyleSheet } from 'react-native';

import defaultStyles from '../config/styles';

function AppText({ children, styles, ...otherProps }) {
  return (
    <Text style={[defaultStyles.text, styles]} {...otherProps}>
      {children}
    </Text>
  );
}

export default AppText;
