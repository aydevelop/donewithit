import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { useFormikContext } from 'formik';

import AppTextInput from './AppTextInput';
import { ErrorMessage } from 'formik';

const AppFormField = ({ name, ...otherProps }) => {
  const { setFieldTouched, handleChange, errors, touched } = useFormikContext();

  return (
    <>
      <AppTextInput
        onChangeText={handleChange(name)}
        onEndEditing={() => {
          setFieldTouched(name);
        }}
        {...otherProps}
        // autoCapitalize='none'
        // icon='email'
        // placeholder='Email'
        // autoCapitalize='none'
        // autoCorrect={false}
        // keyboardType='email-address'
        // textContentType='emailAddress'
      />
      <ErrorMessage error={errors[name]} visible={touched[name]} />
    </>
  );
};

export default AppFormField;

const styles = StyleSheet.create({});
