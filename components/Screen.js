import React from 'react';
import { StyleSheet, SafeAreaView, StatusBar, View } from 'react-native';
import Constants from 'expo-constants';

const Screen = (props) => {
  return (
    <SafeAreaView style={[styles.screen, props.styles]}>
      <StatusBar barStyle='default' hidden={false} />
      {props.children}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
});

export default Screen;
